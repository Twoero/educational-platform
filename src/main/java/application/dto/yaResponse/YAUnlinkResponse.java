package application.dto.yaResponse;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class YAUnlinkResponse {
    @ApiModelProperty(required = true)
    String request_id;
}
