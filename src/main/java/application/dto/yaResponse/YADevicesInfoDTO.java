package application.dto.yaResponse;

import application.dto.devices.YAPayLoad;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class YADevicesInfoDTO {
    @ApiModelProperty(required = true)
    String request_id;

    @ApiModelProperty(required = true)
    YAPayLoad payload;
}
