package application.dto.yaRequests;

import application.dto.devices.YADevice;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class YAInfoStateDevicesRequest {
    @ApiModelProperty
    List<YADevice> devices;
}
