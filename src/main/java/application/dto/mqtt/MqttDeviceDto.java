package application.dto.mqtt;

import application.dto.devices.deviceState.DeviceState;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.time.LocalDateTime;


@Data
@Getter
@Setter
@NoArgsConstructor
@Accessors(chain = true)
public class MqttDeviceDto {

    private String id;

    private DeviceState deviceProperty;

    private LocalDateTime timestamp;

    private String deviceType;

}
