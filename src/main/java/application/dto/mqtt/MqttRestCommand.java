package application.dto.mqtt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RequestBody;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
public class MqttRestCommand {

    private String message;

    private String topic;

}
