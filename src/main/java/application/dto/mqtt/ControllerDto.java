package application.dto.mqtt;


import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Getter
@Setter
@Accessors(chain = true)
public class ControllerDto {

    private String macAddress;

    private String mqttTopic;

    private List<MqttDeviceDto> relayList;

}
