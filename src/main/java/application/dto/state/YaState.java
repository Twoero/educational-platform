package application.dto.state;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class YaState {
    private String instance;
    private Object value;
    private ActionResult action_result;
}
