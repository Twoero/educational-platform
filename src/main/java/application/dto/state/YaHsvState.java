package application.dto.state;

import application.dto.devices.deviceState.DeviceState;
import application.enums.InstanceType;

public class YaHsvState extends YaState {
    private String instance;
    private Object value;

    public YaHsvState(DeviceState state) {
        this.instance = InstanceType.HSV.getTag();
        this.value = new HSV(state);
    }

    public class HSV {
        private Integer h;
        private Integer s;
        private Integer v;

        public HSV(DeviceState rgb) {
            this.h = rgb.getColorH();
            this.s = rgb.getColorS();
            this.v = rgb.getColorB();
        }
    }
}
