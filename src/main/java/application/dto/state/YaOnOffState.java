package application.dto.state;

import application.dto.devices.deviceState.DeviceState;
import application.enums.InstanceType;
import lombok.Data;

@Data
public class YaOnOffState extends YaState {
    private String instance;
    private Object value;

    public YaOnOffState(DeviceState state) {
        this.instance = InstanceType.ON.getTag();
        if ("ON".equals(state.getPower())) {
            this.value = true;
        } else {
            this.value = false;
        }
    }

}
