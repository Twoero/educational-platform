package application.dto.state;

import application.dto.devices.deviceState.DeviceState;

public class YaRangeState extends YaState {

    private String instance;

    private int value;

    public YaRangeState(DeviceState range) {

        this.instance = "brightness";

        this.value = range.getDimmer();

    }

}
