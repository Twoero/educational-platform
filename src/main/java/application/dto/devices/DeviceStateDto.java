package application.dto.devices;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceStateDto {

    private String id;

    private List<YADeviceCapability> capabilities;

    private Map<String, Object> properties;

    private String error_code;

    private String error_message;

}
