package application.dto.devices;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class YADeviceActionResult {
    @ApiModelProperty
    String status;

    @ApiModelProperty
    String error_code;

    @ApiModelProperty
    String error_message;

    public YADeviceActionResult(String status) {
        this.status = status;
    }

}
