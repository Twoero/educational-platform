package application.dto.devices;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.hypersistence.utils.hibernate.type.json.JsonBinaryType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Accessors(chain = true)
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DEV_TYPE")
public class YADevice implements Serializable {

    @Id
    @ApiModelProperty(required = true)
    protected String id;

    @ApiModelProperty
    protected String name;

    @ApiModelProperty
    protected String room;

    @ApiModelProperty
    protected String type;

    @ApiModelProperty
    protected String description;

    @ApiModelProperty
    protected String error_code;

    @ApiModelProperty
    protected String error_message;

    @ApiModelProperty
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    protected Object custom_data;

//    Умение (capability) — свойство объекта Устройство (device),
//    которое описывает его возможности (умения). Устройство описывается как минимум одним умением,
//    если у него при этом нет свойств. Платформа умного дома не ограничивает провайдеров в выборе возможностей
//    для устройств, поэтому у пользователя могут появиться устройства с нестандартной связкой device_type:capability,
//    например, у телевизора может быть функция «температура».
    @ApiModelProperty
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    protected List<YADeviceCapability> capabilities;


    @ApiModelProperty
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    protected Map<String, Object> properties;

    @ApiModelProperty
    @Type(type = "jsonb")
    @Column(columnDefinition = "jsonb")
    protected DeviceInfo device_info;

}
