package application.dto.devices;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.util.List;
import java.util.Map;

@JsonInclude(JsonInclude.Include.NON_NULL)
@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
public class YADeviceDTO {

    private String id;

    private String name;

    private String room;

    private String type;

    private String description;

    private String error_code;

    private String error_message;

    private Object custom_data;

    private List<YADeviceCapability> capabilities;

    private Map<String, Object> properties;

    private DeviceInfo device_info;

}
