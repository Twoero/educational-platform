package application.dto.devices;

import application.dto.state.YaState;
import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class YADeviceCapability {
    @ApiModelProperty(required = true)
    public String type;

    @ApiModelProperty
    public Boolean retrievable;

    @ApiModelProperty
    public String reportable;

//    todo есть ограниченное количество параметров: https://yandex.ru/dev/dialogs/smart-home/doc/concepts/capability-types.html
    @ApiModelProperty
    public Map<String, Object> parameters;

    //    todo есть ограниченное количество параметров: https://yandex.ru/dev/dialogs/smart-home/doc/concepts/capability-types.html
    @ApiModelProperty
    public YaState state;

}
