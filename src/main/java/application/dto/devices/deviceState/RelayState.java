package application.dto.devices.deviceState;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class RelayState extends DeviceState {

}
