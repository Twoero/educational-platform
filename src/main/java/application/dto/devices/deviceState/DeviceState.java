package application.dto.devices.deviceState;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class DeviceState {

    private String power;

    private Integer colorH;

    private Integer colorS;

    private Integer colorB;

    private Integer dimmer;
}
