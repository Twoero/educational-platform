package application.dto.devices;

import com.fasterxml.jackson.annotation.JsonInclude;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Accessors(chain = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DeviceInfo {
    @ApiModelProperty(required = true)
    String manufacturer;

    @ApiModelProperty(required = true)
    String model;

    @ApiModelProperty(required = true)
    String hw_version;

    @ApiModelProperty(required = true)
    String sw_version;
}
