package application.dto.devices.deviceImpl;

import application.dto.devices.DeviceInfo;
import application.dto.devices.YADevice;
import application.dto.devices.YADeviceCapability;
import lombok.NoArgsConstructor;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.List;

@Entity
@DiscriminatorValue("relay")
@NoArgsConstructor
public class YADeviceRelay extends YADevice {


    public YADeviceRelay( String id, List<YADeviceCapability> capabilitiesList){
        this.id = id;
        this.name =  "relay";
        this.room = "default";
        this.type = "devices.types.switch";
        this.description = "new relay";
        this.device_info = new DeviceInfo().setHw_version("1.0")
                .setSw_version("1.0")
                .setManufacturer("Custom")
                .setModel("Custom");
        this.capabilities = capabilitiesList;

    }
}
