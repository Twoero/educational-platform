package application.dto.devices.deviceImpl;

import application.dto.devices.DeviceInfo;
import application.dto.devices.YADevice;
import application.dto.devices.YADeviceCapability;
import application.dto.devices.deviceState.RGBState;
import application.enums.DeviceType;
import lombok.NoArgsConstructor;
import org.modelmapper.ModelMapper;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Entity
@DiscriminatorValue("rgb")
@NoArgsConstructor
public class YADeviceRGB extends YADevice {
    public YADeviceRGB( String id, List<YADeviceCapability> capabilities) {
        this.id = id;
        this.name = name;
        this.room = room;
        this.type = "devices.types.light";
        this.description = "new light";
        this.device_info = new DeviceInfo().setHw_version("1.0")
                .setSw_version("1.0")
                .setManufacturer("Custom")
                .setModel("Custom");
        this.capabilities = capabilities;

    }
}
