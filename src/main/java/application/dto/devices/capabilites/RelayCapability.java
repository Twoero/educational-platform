package application.dto.devices.capabilites;

import application.dto.devices.YADeviceCapability;
import application.dto.devices.deviceState.DeviceState;
import application.dto.state.YaOnOffState;
import application.enums.DeviceCapabilityType;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;


@Data
@Slf4j
@NoArgsConstructor
public class RelayCapability extends YADeviceCapability {

    public RelayCapability (DeviceState relayState) {
        log.info("creating RelayCapability");
        super.type = DeviceCapabilityType.ON_OFF.getTag();
        super.state = new YaOnOffState(relayState);
    }

}
