package application.dto.devices.capabilites;

import application.dto.devices.YADeviceCapability;
import application.dto.devices.deviceState.DeviceState;
import application.dto.state.YaRangeState;
import application.enums.DeviceCapabilityType;
import lombok.Data;

@Data
public class RangeCapability extends YADeviceCapability {

    public RangeCapability (DeviceState deviceState) {
        this.type = DeviceCapabilityType.RANGE.getTag();
        this.state = new YaRangeState(deviceState);
    }

}
