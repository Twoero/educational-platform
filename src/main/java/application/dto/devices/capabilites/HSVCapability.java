package application.dto.devices.capabilites;

import application.dto.devices.YADeviceCapability;
import application.dto.devices.deviceState.DeviceState;
import application.dto.devices.deviceState.RGBState;
import application.dto.state.YaHsvState;
import application.dto.state.YaState;
import application.enums.DeviceCapabilityType;
import org.modelmapper.ModelMapper;

public class HSVCapability extends YADeviceCapability {
    public HSVCapability(DeviceState rgbState) {
        this.type = DeviceCapabilityType.COLOR_SETTING.getTag();
        this.state = new YaHsvState(rgbState);
    }

}
