package application.repository;


import application.dto.devices.YADevice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface YaDeviceRepo extends JpaRepository<YADevice, String> {

    public YADevice getDeviceById(String id);

}
