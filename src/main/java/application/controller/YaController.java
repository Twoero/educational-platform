package application.controller;

import application.dto.devices.YAPayLoad;
import application.dto.yaResponse.YADevicesInfoDTO;
import application.dto.yaResponse.YAUnlinkResponse;
import application.service.DeviceService;
import application.service.RestTemplateService;
import application.service.YaApiService;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.net.URISyntaxException;

//Реализация API Протокола работы платформы умного дома Яндекс
//https://yandex.ru/dev/dialogs/smart-home/doc/ru/reference/resources
@RestController
@Slf4j
public class YaController {

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private YaApiService yaApiService;

    @Autowired
    private RestTemplateService restTemplateService;


    @RequestMapping(value = "/v1.0", method = {RequestMethod.HEAD})
    @ApiOperation(value = "ResponseCheckAvailable",
            notes = "Проверка доступности Endpoint URL провайдера https://yandex.ru/dev/dialogs/smart-home/doc/ru/reference/check")
    public HttpStatus checkAvailable() {
        return HttpStatus.OK;
    }

    @PostMapping(value = "/v1.0/user/unlink")
    @ApiOperation(value = "UnlinkAccount",
            notes = "Оповещение о разъединении аккаунтов https://yandex.ru/dev/dialogs/smart-home/doc/ru/reference/unlink")
    public YAUnlinkResponse unlink(@RequestHeader("X-Request-Id") String id) {
        return new YAUnlinkResponse(id);
    }

    @GetMapping(value = "/v1.0/user/devices")
    @ApiOperation(value = "GetListAllDevices",
            notes = "Информация об устройствах пользователя https://yandex.ru/dev/dialogs/smart-home/doc/ru/reference/get-devices")
    public YADevicesInfoDTO getDevicesList(@RequestHeader("X-Request-Id") String id) {
        try {
            return yaApiService.getDevices(id);
        } catch (URISyntaxException | JsonProcessingException e) {
            return null;
        }
    }

    @PostMapping(value = "/v1.0/user/devices/query")
    @ApiOperation(
            value = "UpdateDevicesStateRequest",
            notes = "Информация о состояниях устройств пользователя https://yandex.ru/dev/dialogs/smart-home/doc/ru/reference/post-devices-query")
    public YADevicesInfoDTO getDevicesStatus(@RequestHeader("X-Request-Id") String id, @RequestBody YAPayLoad devices) {
       try {
           return yaApiService.getDevicesStatus(id, devices);
       } catch (URISyntaxException | JsonProcessingException e) {
           return null;
       }
    }

    @PostMapping(value = "/v1.0/user/devices/action")
    @ApiOperation(value = "DevicesActionRequest",
            notes = "Изменение состояния у устройств https://yandex.ru/dev/dialogs/smart-home/doc/ru/reference/post-action")
    public YADevicesInfoDTO changeDeviceState(@RequestHeader("X-Request-Id") String id, @RequestBody YADevicesInfoDTO requestDTO) {
        return yaApiService.changeDevicesState(id, requestDTO);
    }

}
