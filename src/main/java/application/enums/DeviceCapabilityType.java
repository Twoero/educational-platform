package application.enums;

public enum DeviceCapabilityType {

    ON_OFF("devices.capabilities.on_off"),
    COLOR_SETTING("devices.capabilities.color_setting"),
    RANGE("devices.capabilities.range");

    private final String tag;

    DeviceCapabilityType(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }


}
