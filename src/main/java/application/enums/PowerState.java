package application.enums;

public enum PowerState {

    ON(true),
    OFF(false);

    private final boolean state;

    PowerState(Boolean state) {
        this.state = state;
    }

    public Boolean getState() {
        return state;
    }
}
