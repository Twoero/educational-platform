package application.enums;

public enum InstanceType {
    ON("on"),
    HSV("hsv"),
    RGB("rgb");

    private final String tag;

    InstanceType(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
