package application.enums;

public enum DeviceType {

    DEFAULT("default"),
    POWER("RelayProperty"),
    RelayProperty("RelayProperty"),
    RGBProperty("RGBProperty"),
    DHT_SENSOR("DHT11"),
    DS18B20_SENSOR("TempUnit");

    private final String tag;

    DeviceType(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return tag;
    }
}
