package application.service;

import application.dto.devices.YADevice;
import application.dto.devices.YADeviceDTO;

import java.util.ArrayList;

public interface DeviceService {
    ArrayList<YADevice> findAllDevices();

    public YADevice saveDevice(YADevice device);

    public ArrayList<YADevice> findDevicesInList (ArrayList<String> listId);

    public YADevice getDeviceById(String id);

    public YADeviceDTO updateDeviceState (YADeviceDTO device);

}
