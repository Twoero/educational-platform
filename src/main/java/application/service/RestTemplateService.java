package application.service;


import application.dto.devices.YADeviceDTO;

import application.dto.mqtt.MqttDeviceDto;
import application.dto.mqtt.RelayIdDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.http.HttpStatus;

import java.net.URISyntaxException;
import java.util.List;

public interface RestTemplateService {
    HttpStatus sendMqttCommand(List<YADeviceDTO> command) throws URISyntaxException;

    //    List<RelayDto> getAllDevicesFromMqtt () throws URISyntaxException, JsonProcessingException;
    List<MqttDeviceDto> getAllDevicesFromMqtt() throws URISyntaxException, JsonProcessingException;

    MqttDeviceDto getDeviceById(RelayIdDto relayId) throws URISyntaxException;

}