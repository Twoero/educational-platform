package application.service;

import application.dto.devices.YADevice;
import application.dto.devices.deviceState.DeviceState;
import application.enums.DeviceType;

public interface YaDeviceFactory {

    YADevice createNewYaDeviceOfType (DeviceType type, String id, DeviceState state);

}
