package application.service;

import application.dto.devices.YADeviceCapability;
import application.dto.devices.deviceState.DeviceState;
import application.enums.DeviceCapabilityType;

import java.util.List;

public interface YACapabilityFactory {
    List<YADeviceCapability> createCapabilities(DeviceCapabilityType type, DeviceState state);
}
