package application.service;

import application.dto.devices.YADevice;
import application.dto.devices.YADeviceDTO;
import application.dto.devices.YAPayLoad;
import application.dto.mqtt.MqttDeviceDto;
import application.dto.state.ActionResult;
import application.dto.yaResponse.YADevicesInfoDTO;
import application.enums.DeviceType;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Service
@Slf4j
public class YaApiService {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private DeviceService deviceService;

    @Autowired
    private RestTemplateService restTemplateService;

    @Autowired
    private YaDeviceFactory yaDeviceFactory;


    public YADevicesInfoDTO getDevices(String requestId) throws URISyntaxException, JsonProcessingException {

        List<MqttDeviceDto> mqttDevices = restTemplateService.getAllDevicesFromMqtt();
        List<YADevice> deviceListFromDb = deviceService.findAllDevices();
        List<YADevice> deviceListResponse = new ArrayList<>();
        List<String> yaDeviceIds = deviceListFromDb.stream().map(YADevice::getId).collect(Collectors.toList());
        log.info("post request returns" + mqttDevices.toString());
        LocalDateTime currentTime = LocalDateTime.now();

        //добавляем все устройства которые получили в ответ, обновляем их состояние в базе
        mqttDevices.forEach(d ->
        {
            //добавляем актуальные устройства по которым есть свежие данные
            if (Math.abs(ChronoUnit.SECONDS.between(currentTime, d.getTimestamp()))<30) {

                DeviceType deviceType;
                try {
                    deviceType = DeviceType.valueOf(d.getDeviceType());
                    YADevice newYaDevice = yaDeviceFactory.createNewYaDeviceOfType(deviceType, d.getId(), d.getDeviceProperty());
                    log.info(newYaDevice.toString());
                    log.info(newYaDevice.toString());
                    deviceListResponse.add(deviceService.saveDevice(newYaDevice));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        });

        //для тех устройств которые есть в базе в yadevice, но не пришли в ответе от модуля mqtt
        //добавляем их в ответ, кладем им в ошибку инфо о том что связь с устройством потеряна или устройство удалено
        //обновляем их состояние в бд
        deviceListFromDb.forEach(d -> {
            if (!deviceListResponse.isEmpty() && !deviceListResponse.stream().map(YADevice::getId)
                    .collect(Collectors.toList()).contains(d.getId())) {
                try {
                    d.setError_code("Connection lost");
                    d.setError_message("Устройство перестало выходить на связь или было удалено в настройках контроллера");
                    deviceListResponse.add(deviceService.saveDevice(d));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });


        ArrayList<YADeviceDTO> deviceDTOList = new ArrayList<>();

        if (!deviceListResponse.isEmpty()) {
            deviceListResponse.forEach(d -> {
                deviceDTOList.add(modelMapper.map(d, YADeviceDTO.class));
            });
        }

        YAPayLoad yaPayLoad = new YAPayLoad();
        yaPayLoad.setUser_id("Misha-01-super-545")
                .setDevices(deviceDTOList);

        YADevicesInfoDTO yaDevicesInfoDTO = new YADevicesInfoDTO();
        yaDevicesInfoDTO.setPayload(yaPayLoad)
                .setRequest_id(requestId);

        return yaDevicesInfoDTO;
    }

    public YADevicesInfoDTO getDevicesStatus(String requestId, YAPayLoad devices)
            throws URISyntaxException, JsonProcessingException {

        List<MqttDeviceDto> mqttDevices = restTemplateService.getAllDevicesFromMqtt();
        //добавляем все устройства которые получили в ответ, обновляем их состояние в базе
        mqttDevices.forEach(d ->
        {
            log.info(d.toString());

            DeviceType deviceType;
            try {
                deviceType = DeviceType.valueOf(d.getDeviceType());
                YADevice newYaDevice = yaDeviceFactory.createNewYaDeviceOfType(deviceType, d.getId(), d.getDeviceProperty());
                log.info(newYaDevice.toString());
                log.info(newYaDevice.toString());
                deviceService.saveDevice(newYaDevice);
            } catch (IllegalArgumentException e) {
                e.printStackTrace();
            }
        });

        ArrayList<String> ids = new ArrayList<>();
        ArrayList<YADeviceDTO> deviceDTOList = new ArrayList<>();
        devices.getDevices().forEach(d -> ids.add(d.getId()));
        ArrayList<YADevice> deviceList = deviceService.findDevicesInList(ids);

        if (!deviceList.isEmpty()) {
            deviceList.forEach(d -> {
                deviceDTOList.add(modelMapper.map(d, YADeviceDTO.class));
            });
        }

        YAPayLoad yaPayLoad = new YAPayLoad();
        yaPayLoad.setDevices(deviceDTOList);

        YADevicesInfoDTO yaDevicesInfoDTO = new YADevicesInfoDTO();
        yaDevicesInfoDTO.setPayload(yaPayLoad)
                .setRequest_id(requestId);

        return yaDevicesInfoDTO;
    }

    public YADevicesInfoDTO changeDevicesState(String requestId, YADevicesInfoDTO devices) {

        ArrayList<YADeviceDTO> deviceList = new ArrayList<>();

        try {
            restTemplateService.sendMqttCommand(devices.getPayload().getDevices());
        } catch (URISyntaxException e) {
            log.error(e.getMessage());
        }

        devices.getPayload().getDevices().forEach(d -> {
            YADeviceDTO devDTO = deviceService.updateDeviceState(d);
            Map<String, String> okActionResult = new HashMap<>();
            okActionResult.put("status", "DONE");
            devDTO.setDevice_info(null)
                    .setName(null)
                    .setRoom(null)
                    .setType(null)
                    .setDescription(null)
                    .setCustom_data(null)
                    .setProperties(null)
                    .getCapabilities().forEach(c -> {
                        c.getState().setAction_result(new ActionResult().setStatus("DONE"));
                        c.getState().setValue(null);
                    });
            deviceList.add(devDTO);
        });

        YAPayLoad yaPayLoad = new YAPayLoad();
        yaPayLoad.setDevices(deviceList);

        YADevicesInfoDTO yaDevicesInfoDTO = new YADevicesInfoDTO();
        yaDevicesInfoDTO.setPayload(yaPayLoad)
                .setRequest_id(requestId);

        return yaDevicesInfoDTO;
    }


}

