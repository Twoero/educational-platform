package application.service.impl;

import application.dto.devices.YADevice;
import application.dto.devices.YADeviceDTO;
import application.dto.mqtt.MqttRestCommand;
import application.dto.mqtt.MqttDeviceDto;
import application.dto.mqtt.RelayIdDto;
import application.service.RestTemplateService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

@Slf4j
@Service
public class RestTemplateServiceImpl implements RestTemplateService {

    @Value("${mqtt.base.url}")
    private String mqttBaseUrl;

    @Value("${mqtt.api.sendCommand}")
    private String sendCommandUrl;

    @Value("${mqtt.api.getAllDevices}")
    private String getAllDevicesUrl;

    @Value("${mqtt.api.getDeviceById}")
    private String getDeviceByIdUrl;

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public HttpStatus sendMqttCommand(List<YADeviceDTO> command) throws URISyntaxException {
        RequestEntity<List<YADeviceDTO>> requestEntity  = RequestEntity
                .post(new URI(mqttBaseUrl+sendCommandUrl))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .body(command);

        ResponseEntity<HttpStatus> responseEntity = restTemplate.exchange(mqttBaseUrl+sendCommandUrl,
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<HttpStatus>() {
                });
        log.info(String.format("Post request to %s returns: %s", sendCommandUrl, responseEntity.getStatusCode()));
        return responseEntity.getStatusCode();
    }

    @Override
    public List<MqttDeviceDto> getAllDevicesFromMqtt() throws URISyntaxException, JsonProcessingException {
        MqttRestCommand command = new MqttRestCommand("test","test");
        ObjectMapper mapper = new ObjectMapper();
        ObjectNode obj = mapper.createObjectNode();

        String s = mapper.writeValueAsString(obj);

        RequestEntity<MqttRestCommand> requestEntity  = RequestEntity
                .post(new URI(mqttBaseUrl+getAllDevicesUrl))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .body(command);

        ResponseEntity<List<MqttDeviceDto>> responseEntity = restTemplate.exchange(mqttBaseUrl+getAllDevicesUrl,
                HttpMethod.POST,
                requestEntity,
                new ParameterizedTypeReference<List<MqttDeviceDto>>() {
                });

//        ResponseEntity<List<RelayDto>> responseEntity = restTemplate.getForEntity(mqttBaseUrl+getAllDevicesUrl,
//                new ParameterizedTypeReference<List<RelayDto>>());
        log.info(String.format("Post request to %s returns: %s", getAllDevicesUrl, responseEntity.getBody()));
        return responseEntity.getBody();
        //        return responseEntity.getBody();
    }

    @Override
    public MqttDeviceDto getDeviceById(RelayIdDto relayId) throws URISyntaxException {
        RequestEntity<RelayIdDto> requestEntity = RequestEntity
                .post(new URI(mqttBaseUrl+getDeviceByIdUrl))
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .body(relayId);

        ResponseEntity<MqttDeviceDto> responseEntity = restTemplate.exchange(mqttBaseUrl+getDeviceByIdUrl,
                HttpMethod.POST,
                requestEntity,
                MqttDeviceDto.class);
        log.info(String.format("Post request to %s returns: %s", getDeviceByIdUrl, responseEntity.getBody()));
        return responseEntity.getBody();
    }
}
