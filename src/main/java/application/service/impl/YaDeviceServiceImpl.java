package application.service.impl;

import application.dto.devices.YADevice;
import application.dto.devices.YADeviceDTO;
import application.repository.YaDeviceRepo;
import application.service.DeviceService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
@Slf4j
@RequiredArgsConstructor
public class YaDeviceServiceImpl implements DeviceService {

    @Autowired
    private YaDeviceRepo yaDeviceRepo;

    @Autowired
    private ModelMapper modelMapper;

    @Override
    public YADevice saveDevice(YADevice device) {
        log.info("try saving new device");
        Optional<YADevice> devices = yaDeviceRepo.findById(device.getId());
        if (devices.isPresent()) {
            log.info("updating device with id: " + device.getId());
        } else {
            log.info("created new device of type" + device.getType());
        }
        return yaDeviceRepo.save(device);
    }

    @Override
    public ArrayList<YADevice> findAllDevices() {
        try {
            ArrayList<YADevice> devices = (ArrayList<YADevice>) yaDeviceRepo.findAll();
            return devices;
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return null;
    }

    @Override
    public YADeviceDTO updateDeviceState(YADeviceDTO device) {
        try {
            YADevice existingDevice = yaDeviceRepo.getDeviceById(device.getId());
            existingDevice.getCapabilities().forEach(c -> {
                device.getCapabilities().forEach(c1 -> {
                    if (c1.getType().equals(c.getType())) {
                        c.setState(c1.getState());
                    }
                });
            });
           return modelMapper.map(yaDeviceRepo.save(existingDevice), YADeviceDTO.class);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    @Override
    public YADevice getDeviceById(String id) {
        try {
            return yaDeviceRepo.getDeviceById(id);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }

    @Override
    public ArrayList<YADevice> findDevicesInList(ArrayList<String> listId) {
        try {
            return (ArrayList<YADevice>) yaDeviceRepo.findAllById(listId);
        } catch (Exception e) {
            log.error(e.getMessage());
            return null;
        }
    }
}
