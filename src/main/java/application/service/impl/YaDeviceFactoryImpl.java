package application.service.impl;

import application.dto.devices.YADevice;
import application.dto.devices.YADeviceCapability;
import application.dto.devices.deviceImpl.YADeviceRGB;
import application.dto.devices.deviceImpl.YADeviceRelay;
import application.dto.devices.deviceState.DeviceState;
import application.enums.DeviceCapabilityType;
import application.enums.DeviceType;
import application.service.YACapabilityFactory;
import application.service.YaDeviceFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class YaDeviceFactoryImpl implements YaDeviceFactory {

    @Autowired
    private YACapabilityFactory capabilityFactory;

    @Override
    public YADevice createNewYaDeviceOfType(DeviceType type, String id, DeviceState state) {
        YADevice device = null;

        switch (type) {
            case RelayProperty:
               List<YADeviceCapability> relayCapabilities = capabilityFactory.createCapabilities(DeviceCapabilityType.ON_OFF, state);
               device = new YADeviceRelay( id, relayCapabilities);
               break;
            case RGBProperty:
                List<YADeviceCapability> rgbCapabilities = capabilityFactory.createCapabilities(DeviceCapabilityType.COLOR_SETTING, state);
                device = new YADeviceRGB( id, rgbCapabilities);
                break;
            default:
                device = null;
                break;
        }
        return device;
    }


}
