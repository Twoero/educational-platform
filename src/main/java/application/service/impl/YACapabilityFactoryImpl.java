package application.service.impl;

import application.dto.devices.YADeviceCapability;
import application.dto.devices.capabilites.HSVCapability;
import application.dto.devices.capabilites.RelayCapability;
import application.dto.devices.deviceState.DeviceState;
import application.enums.DeviceCapabilityType;
import application.service.YACapabilityFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class YACapabilityFactoryImpl implements YACapabilityFactory {

    @Override
    public List<YADeviceCapability> createCapabilities(DeviceCapabilityType type, DeviceState state) {

        List<YADeviceCapability> capabilities = new ArrayList<>();

        switch (type) {
            case ON_OFF:
                capabilities.add(createOnOffCapability(state));
                log.info(String.format("create ON_OFF capabilities: %s", capabilities.toString()));
                break;
            case COLOR_SETTING:
                capabilities.add(createOnOffCapability(state));
                capabilities.add(createHsvCapability(state));
                capabilities.add(createRangeCapability(state));
                log.info(String.format("create COLOR_SETTING capabilities: %s", capabilities.toString()));
                break;
            case RANGE:
                capabilities.add(createRangeCapability(state));
                log.info(String.format("create RANGE capabilities: %s", capabilities.toString()));
                break;
        }
        return capabilities;
    }

    private YADeviceCapability createOnOffCapability(DeviceState state) {
        return new RelayCapability(state);
    }

    private YADeviceCapability createHsvCapability(DeviceState state) {
        return new HSVCapability(state);
    }

    private YADeviceCapability createRangeCapability(DeviceState state) {
        return new HSVCapability(state);
    }
}
